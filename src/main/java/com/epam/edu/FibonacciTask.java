package com.epam.edu;

import static com.epam.edu.Util.*;

import java.util.Scanner;

/**
 * Contains methods related to FibonacciTask.
 */
final class FibonacciTask {

  /**
   * Empty constructor.
   */
  private FibonacciTask() {

  }

  /**
   * Executes Fibonacci task.
   */
  static void executeTask() {
    // Program build Fibonacci numbers: F1 will be the biggest odd number and
    // F2 – the biggest even number, user can enter the size of set (N);
    System.out.println("Enter number of Fibonacci numbers: ");
    int[] fibNumbers = getFibNumbers(
        safeNextInt(new Scanner(System.in, "UTF-8")));
    System.out.println("Sequence: ");
    printArrayFromStart(fibNumbers);

    int F1 = maxNumberInArray(getOddNumbers(fibNumbers));
    int F2 = maxNumberInArray(getEvenNumbers(fibNumbers));

    System.out.println("Biggest odd number from Fibonacci sequence: " + F1);
    System.out.println("Biggest even number from Fibonacci sequence: " + F2);

    // Program prints percentage of odd and even Fibonacci numbers
    System.out.print("\nPercentage of odd numbers: ");
    System.out.printf("%.2f",
        getPercentage(getOddNumbers(fibNumbers).length, fibNumbers.length));
    System.out.print("\nPercentage of even numbers: ");
    System.out.printf("%.2f",
        getPercentage(getEvenNumbers(fibNumbers).length, fibNumbers.length));
  }

  /**
   * Generates Fibonacci sequence.
   *
   * @param number size of sequence
   * @return array containing sequence
   */
  private static int[] getFibNumbers(final int number) {
    int[] result = new int[number];
    result[0] = 0;
    result[1] = 1;

    for (int i = 2; i < number; i++) {
      result[i] = result[i - 1] + result[i - 2];
    }

    return result;
  }

}
