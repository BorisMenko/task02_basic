package com.epam.edu;

import static com.epam.edu.Util.*;

import java.util.Scanner;

/**
 * Contains methods related to IntervalTask.
 */
final class IntervalTask {

  /**
   * Empty constructor.
   */
  private IntervalTask() {
  }

  /**
   * Interval start.
   */
  private static int intervalStart = 1;
  /**
   * Interval end.
   */
  private static int intervalEnd = 0;

  /**
   * Executes interval task.
   */
  static void executeTask() {
    // User enter the interval (for example: [1;100]);
    enterInterval(new Scanner(System.in, "UTF-8"));
    int[] array = getArrayFromInterval();

    int[] evenNumbers = getEvenNumbers(array);
    int[] oddNumbers = getOddNumbers(array);

    // Program prints odd numbers from start to the end of interval
    // and even from end to start;
    System.out.println("Odd numbers from start.");
    printArrayFromStart(oddNumbers);
    System.out.println("Even numbers from end.");
    printArrayFromEnd(evenNumbers);

    // Program prints the sum of odd and even numbers;
    int sumOfEven = getSumOfElementsInArray(evenNumbers);
    int sumOfOdd = getSumOfElementsInArray(oddNumbers);

    System.out.println("Sum of even: " + sumOfEven);
    System.out.println("Sum of odd: " + sumOfOdd);
  }


  /**
   * Reads start and end of interval from standart input.
   *
   * @param sc text scanner
   */
  private static void enterInterval(final Scanner sc) {
    do {
      System.out.println("Enter interval start: ");
      intervalStart = safeNextInt(sc);

      System.out.println("Enter interval end: ");
      intervalEnd = safeNextInt(sc);

      if (intervalStart < intervalEnd) {
        break;
      }
      System.out.println("Please enter correct interval");

    } while (true);
  }

  /**
   * Generates array from interval.
   *
   * @return resulting array
   */
  private static int[] getArrayFromInterval() {
    int[] result = new int[intervalEnd - intervalStart + 1];

    for (int i = 0; i < result.length; i++) {
      result[i] = intervalStart;
      intervalStart++;
    }

    return result;
  }
}
